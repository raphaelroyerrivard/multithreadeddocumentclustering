import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class KMean {
    private static final long seed = 548008610721L;
    private static final String dataFilename = "transactions.dat";
    private static final String resultFilename = "result.dat";
    private static final float epsilonDist = 0.0001f;
    private static final float epsilonChange = 0.01f;

    private static int clusterCount = 653;
    private static int maxIteration = 30;
    private static int documentCount;
    private static int itemCount;
    private static int[][] transactions;
    private static int[] clusterMatrix, oldClusterMatrix;
    private static float[][] centroids, oldCentroids;

    public static void main(String[] args) throws IOException {
        readData();
        initializeCentroids();
        validateCentroids();
        int iterationCount = 0;
        long then;
        while (true) {
            then = System.nanoTime();
            saveOldClusterMatrix();
            computeClusters();
            updateCentroids();
            if (countClusterChanges()) break;
            if (converge()) break;
            if (--maxIteration <= 0) break;
            log("End of iteration " + ++iterationCount + " " + getElapsed(then));
        }
        log("End of iteration " + ++iterationCount + " " + getElapsed(then));
        writeResults();
    }

    private static void saveOldClusterMatrix() {
        int[] temp = oldClusterMatrix;
        oldClusterMatrix = clusterMatrix;
        clusterMatrix = temp;
    }

    private static String getElapsed(long then) {
        long elapsed = System.nanoTime() - then;
        long milli = elapsed / 1000000;
        long seconds = milli / 1000;
        milli -= seconds * 1000;
        long minutes = seconds / 60;
        seconds -= minutes * 60;
        long hours = minutes / 60;
        minutes -= hours * 60;
        return String.format("%02d:%02d:%02d.%03d", hours, minutes, seconds, milli);
    }

    private static void validateCentroids() {
        Set<List<Float>> unique = new HashSet<>();
        int duplicateCount = 0;
        for (int i = 0; i < clusterCount; i++) {
            List<Float> centroid = new ArrayList<>();
            for (int j = 0; j < itemCount; j++)
                centroid.add(centroids[i][j]);
            if (unique.contains(centroid))
                duplicateCount++;
            else
                unique.add(centroid);
        }
        log("Found " + duplicateCount + " duplicate centroid");
    }

    private static void writeResults() throws IOException {
        log("Writing results");
        FileOutputStream file = new FileOutputStream(resultFilename);
        BufferedOutputStream buffer = new BufferedOutputStream(file);
        DataOutputStream writer = new DataOutputStream(buffer);

        writer.writeInt(clusterCount);
        writer.writeInt(itemCount);
        for (int i = 0; i < clusterCount; ++i) {
            for (int j = 0; j < itemCount; ++j) {
                writer.writeFloat(centroids[i][j]);
            }
        }

        writer.writeInt(documentCount);
        for (int i = 0; i < documentCount; i++) {
            writer.writeInt(clusterMatrix[i]);
        }

        writer.close();
        log("Done.");
    }

    private static boolean countClusterChanges() {
        log("Counting cluster changes");
        int count = 0;
        for (int i = 0; i < documentCount; i++) {
            if (oldClusterMatrix[i] != clusterMatrix[i])
                count++;
        }
        float ratio = (float) count / (float) documentCount;
        log(String.format("Done. %.1f%% %d documents changed cluster", ratio * 100, count));
        return ratio <= epsilonChange;
    }

    private static boolean converge() {
        log("Computing convergence");
        float maxDist = 0;
        for (int i = 0; i < clusterCount; i++) {
            float dist = dist(oldCentroids[i], centroids[i]);
            if (maxDist < dist)
                maxDist = dist;
        }
        log("Done max centroid change: " + maxDist);
        return maxDist < epsilonDist;
    }

    private static void updateCentroids() {
        log("Updating centroids");

        // Initialization
        oldCentroids = centroids;
        centroids = new float[clusterCount][];
        for (int i = 0; i < clusterCount; i++) {
            centroids[i] = new float[itemCount];
        }
        int[] clusterSize = new int[clusterCount];

        // Summation
        for (int i = 0; i < documentCount; i++) {
            int cluster = clusterMatrix[i];
            for (int j = 0; j < itemCount; j++) {
                centroids[cluster][j] += transactions[i][j];
            }
            clusterSize[cluster]++;
        }

        int faultCount = 0;
        // Average
        for (int i = 0; i < clusterCount; i++) {
            if (clusterSize[i] > 0)
                for (int j = 0; j < itemCount; j++) {
                    centroids[i][j] /= clusterSize[i];
                }
            else
                faultCount++;
        }
        log("Done. " + faultCount + " empty clusters");
    }

    private static void computeClusters() {
        log("Computing clusters");
        spotOnCount = 0;
        for (int i = 0; i < documentCount; i++) {
            clusterMatrix[i] = closest(transactions[i]);
        }
        log("Spot on count " + spotOnCount);
        log("Done");
    }

    private static int spotOnCount;

    private static int closest(int[] transaction) {
        int cluster = 0;
        float minDist = dist(transaction, centroids[cluster]);

        for (int i = 1; i < clusterCount; i++) {
            float dist = dist(transaction, centroids[i]);
            if (dist == 0)
                spotOnCount++;
            if (minDist > dist) {
                minDist = dist;
                cluster = i;
            }
        }
        return cluster;
    }

    private static void initializeCentroids() {
        log("Selecting random centroids");
        centroids = new float[clusterCount][];
        Random rng = new Random(seed);
        Set<Integer> idx = new LinkedHashSet<>();
        for (int i = 0; i < clusterCount; i++) {
            int index;
            do
                index = rng.nextInt(documentCount);
            while (idx.contains(index));
            idx.add(index);
            centroids[i] = new float[itemCount];
            for (int j = 0; j < itemCount; j++)
                centroids[i][j] = transactions[index][j];
        }
        log("Done");
    }

    private static float dist(int[] item1, float[] item2) {
        float dist = 0;
        for (int i = 0; i < itemCount; ++i) {
            float sub = item1[i] - item2[i];
            dist += sub * sub;
        }
        return dist;
    }

    private static float dist(float[] item1, float[] item2) {
        float dist = 0;
        for (int i = 0; i < itemCount; ++i) {
            float sub = item1[i] - item2[i];
            dist += sub * sub;
        }
        return dist;
    }

    private static void readData() throws IOException {
        FileInputStream file = new FileInputStream(dataFilename);
        BufferedInputStream buffer = new BufferedInputStream(file);
        DataInputStream reader = new DataInputStream(buffer);

        log("Reading Header");

        documentCount = reader.readInt();
        log("Transaction count: " + documentCount);
        reader.skipBytes(documentCount * 4);

        itemCount = reader.readInt();
        log("Item count: " + itemCount);
        reader.skipBytes(itemCount * 4);

        log("Reading Data");

        transactions = new int[documentCount][itemCount];
        clusterMatrix = new int[documentCount];
        oldClusterMatrix = new int[documentCount];

        for (int i = 0; i < documentCount; ++i) {
            int nbEntry = reader.readInt();
            for (int j = 0; j < nbEntry; ++j) {
                int wordId = reader.readInt();
                transactions[i][wordId] = reader.readInt();
            }
        }

        log("Done.");
    }

    private static void log(String msg) {
        System.out.printf("[%s] %s \n", getTimeStamp(), msg);
    }

    private static String getTimeStamp() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS"));
    }
}

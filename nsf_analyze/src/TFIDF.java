import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;
import java.util.stream.Collectors;

public class TFIDF {

    public static void main(String[] args) {
        try {
            HashMap<Integer, Integer> wordDocs = new HashMap<>();
            List<HashMap<Integer, Integer>> docWordsFrequency = new ArrayList<>();
            List<HashMap<Integer, Double>> docWordsScore = new ArrayList<>();
            List<List<Map.Entry<Integer, Double>>> orderedDocWords = new ArrayList<>();
            HashMap<Integer, Double> bestScores = new HashMap<>();
            HashMap<Integer, Integer> importantWordsFrequency = new HashMap<>();
            FileReader file;
            FileWriter fileWriter;
            BufferedReader reader;
            BufferedWriter writer;
            String line;
            int docId, wordId, wordCount, docCount;
            double wordScore;
            for(int i=1; i<=3; ++i) {
                System.out.println("Reading file " + i);
                file = new FileReader("../nsfabs_part" + i + "_out/docwords.txt");
                reader = new BufferedReader(file);
                while ((line = reader.readLine()) != null) {
                    String[] values = line.split(" ");
                    docId = Integer.valueOf(values[0]);
                    wordId = Integer.valueOf(values[1]);
                    wordCount = Integer.valueOf(values[2]);
                    docCount = wordDocs.getOrDefault(wordId, 0);
                    wordDocs.put(wordId, docCount + 1);
                    if (docWordsFrequency.size() < docId) {
                        docWordsFrequency.add(new HashMap<>());
                    }
                    docWordsFrequency.get(docId - 1).put(wordId, wordCount);
                }
                reader.close();
            }
            System.out.println("Calculating scores");
            for(int i=0; i<docWordsFrequency.size(); ++i) {
                HashMap<Integer, Integer> wordsFrequency = docWordsFrequency.get(i);
                HashMap<Integer, Double> wordsScore = new HashMap<>();
                for(Integer word : wordsFrequency.keySet()) {
                    wordScore = wordsFrequency.get(word) * Math.log(docWordsFrequency.size() / wordDocs.get(word));
                    wordsScore.put(word, wordScore);
                }
                docWordsScore.add(wordsScore);
                List<Map.Entry<Integer, Double>> docBestWords = wordsScore.entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                        .collect(Collectors.toList());
                orderedDocWords.add(docBestWords);
                for(Map.Entry<Integer, Double> word : docBestWords) {
                    if(word.getValue() > bestScores.getOrDefault(word.getKey(), 0.0))
                        bestScores.put(word.getKey(), word.getValue());
                }
            }
            System.out.println("Keeping most important words");
            List<Map.Entry<Integer, Double>> sortedBestScores = bestScores.entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                    .collect(Collectors.toList())
                    .subList(0, 5000);
            sortedBestScores.sort(Map.Entry.comparingByKey());
            List<Integer> sortedBestScoresList = new ArrayList<>();
            for(Map.Entry<Integer, Double> entry : sortedBestScores)
                sortedBestScoresList.add(entry.getKey());
            for(List<Map.Entry<Integer, Double>> words : orderedDocWords) {
                //only consider the highest scored word of each document
                for(int i=0; i<1; ++i) {
                    wordId = words.get(0).getKey();
                    wordCount = importantWordsFrequency.getOrDefault(wordId, 0);
                    importantWordsFrequency.put(wordId, wordCount + 1);
                }
            }
            List<Map.Entry<Integer, Integer>> mostFrequentImportantWords = importantWordsFrequency.entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                    .collect(Collectors.toList())
                    .subList(0, 5000);
            mostFrequentImportantWords.sort(Map.Entry.comparingByKey());
            List<Integer> mostFrequentImportantWordsList = new ArrayList<>();
            for(Map.Entry<Integer, Integer> entry : mostFrequentImportantWords)
                mostFrequentImportantWordsList.add(entry.getKey());
            System.out.println("Writing best words in files");
            fileWriter = new FileWriter("../words_by_score.txt");
            writer = new BufferedWriter(fileWriter);
            for(Map.Entry<Integer, Double> word : sortedBestScores)
                writer.write(String.valueOf(word.getKey()) + " " + String.valueOf(word.getValue()) + "\n");
            writer.close();
            fileWriter = new FileWriter("../words_by_frequency.txt");
            writer = new BufferedWriter(fileWriter);
            for(Map.Entry<Integer, Integer> word : mostFrequentImportantWords)
                writer.write(String.valueOf(word.getKey()) + " " + String.valueOf(word.getValue()) + "\n");
            writer.close();
            System.out.println("Creating cleaned docwords file");
            int ignoredDocsCount = 0;
            //both files cannot be generated at the same time because it alters the docWordsFrequency list
            //fileWriter = new FileWriter("../cleaned_doc_words_by_score.txt");
            fileWriter = new FileWriter("../cleaned_doc_words_by_frequency.txt");
            writer = new BufferedWriter(fileWriter);
            for(int i=0; i<docWordsFrequency.size(); ++i) {
                HashMap<Integer, Integer> wordsFrequency = docWordsFrequency.get(i);
                List<Integer> wordsToRemove = wordsFrequency.keySet()
                        .parallelStream()
                        //.filter(word -> !sortedBestScoresList.contains(word))
                        .filter(word -> !mostFrequentImportantWordsList.contains(word))
                        .collect(Collectors.toList());
                wordsFrequency.keySet().removeAll(wordsToRemove);
                if(wordsFrequency.isEmpty())
                    ++ignoredDocsCount;
                for(Map.Entry<Integer, Integer> word : wordsFrequency.entrySet())
                    writer.write(String.valueOf(i + 1) + " " + String.valueOf(word.getKey()) + " " + String.valueOf(word.getValue()) + "\n");
            }
            writer.close();
            System.out.println(ignoredDocsCount + " documents not represented");
            /*System.out.println("Comparing results of the 2 methods");
            for(Map.Entry<Integer, Double> scoreWord : sortedBestScores) {
                Map.Entry<Integer, Integer> found = null;
                for(Map.Entry<Integer, Integer> frequencyWord : mostFrequentImportantWords) {
                    if(scoreWord.getKey().equals(frequencyWord.getKey())) {
                        found = frequencyWord;
                        break;
                    }
                }
                if(found != null)
                    mostFrequentImportantWords.remove(found);
            }
            System.out.println(mostFrequentImportantWords.size() + " words differ");*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

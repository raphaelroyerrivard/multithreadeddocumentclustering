import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class NSFPrograms {
    public static void main(String[] args) {
        try {
            HashMap<String, Integer> docIds = new HashMap<>();
            List<String> docPrograms = new ArrayList<>();
            HashMap<String, String> nsfPrograms = new HashMap<>();
            File[] years, groups, awards;
            FileReader fileReader;
            FileWriter fileWriter;
            BufferedReader reader;
            BufferedWriter writer;
            String[] idAndProgram, ids, fileName;
            String path, line, programId, programName, docProgram = "";
            //Read doc ids
            for(int i=1; i<=3; ++i) {
                fileReader = new FileReader("../nsfabs_part" + i + "_out/idnsfid.txt");
                reader = new BufferedReader(fileReader);
                while ((line = reader.readLine()) != null) {
                    ids = line.split("\\t");
                    docIds.put(ids[1], Integer.parseInt(ids[0]));
                }
            }
            for(int i=1; i<=3; ++i) {
                path = "../Part" + i;
                years = new File(path).listFiles();
                for(File year : years) {
                    System.out.println(year.getName());
                    groups = year.listFiles();
                    for(File group : groups) {
                        if(group.isDirectory()) {
                            awards = group.listFiles();
                            for(File award : awards) {
                                fileName = award.getName().split("\\.");
                                if(fileName[1].equals("txt") && docIds.containsKey(fileName[0])) {
                                    fileReader = new FileReader(award);
                                    reader = new BufferedReader(fileReader);
                                    while ((line = reader.readLine()) != null) {
                                        if (line.startsWith("NSF Program")) {
                                            docProgram = line.split(" : ")[1];
                                            break;
                                        }
                                    }
                                    reader.close();
                                    idAndProgram = docProgram.split("      ");
                                    programId = idAndProgram[0];
                                    programName = idAndProgram.length > 1 ? idAndProgram[1] : "N/A";
                                    docPrograms.add(programId);
                                    nsfPrograms.put(programId, programName);
                                    /*if(programName.contains("DATA NOT AVAILABLE"))
                                        System.out.println("WTF");*/
                                }
                            }
                        }
                    }
                }
            }
            System.out.println("Writing doc programs in file");
            fileWriter = new FileWriter("../docprograms.txt");
            writer = new BufferedWriter(fileWriter);
            for(int i=0; i<docPrograms.size(); ++i)
                writer.write((i + 1) + " " + docPrograms.get(i) + "\n");
            writer.close();
            //sort NSF programs by id
            List<Map.Entry<String, String>> sortedPrograms = nsfPrograms.entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toList());
            fileWriter = new FileWriter("../nsfprograms.txt");
            writer = new BufferedWriter(fileWriter);
            for(Map.Entry<String, String> program : sortedPrograms)
                writer.write(program.getKey() + " " + program.getValue() + "\n");
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

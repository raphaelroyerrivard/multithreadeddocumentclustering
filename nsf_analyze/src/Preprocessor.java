import java.io.*;
import java.util.*;

public class Preprocessor {
    private static final String transactionsFileName = "../cleaned_doc_words_by_frequency.txt";
    private static final String itemsFileName = "../words_by_frequency.txt";

    public static void main(String[] args) {
        try {
            new Preprocessor().run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static ArrayList<Integer> documentsId = new ArrayList<>();
    private static ArrayList<Integer> itemsId = new ArrayList<>();
    private static HashMap<Integer, HashMap<Integer, Integer>> transactions = new HashMap<>();

    private void run() throws IOException {
        System.out.println("Reading transactions from " + transactionsFileName);
        readTransactions();
        System.out.println("Found " + documentsId.size() + " transactions");
        System.out.println("Reading items from " + itemsFileName);
        readItems();
        System.out.println("Found " + itemsId.size() + " items");
        System.out.println("Starting to write data file");
        writeTransactionData();
        System.out.println("Done");
    }

    private void readTransactions() throws IOException {
        FileReader file;
        BufferedReader reader;
        String line;

        int docId, wordId, wordCount;

        file = new FileReader(transactionsFileName);
        reader = new BufferedReader(file);
        while ((line = reader.readLine()) != null) {
            String[] values = line.split(" ");
            docId = Integer.valueOf(values[0]);
            wordId = Integer.valueOf(values[1]);
            wordCount = Integer.valueOf(values[2]);

            if (!transactions.containsKey(docId)) {
                transactions.put(docId, new HashMap<>());
                documentsId.add(docId);
            }
            transactions.get(docId).put(wordId, wordCount);
        }
        reader.close();
    }

    private void readItems() throws IOException {
        Integer wordId;
        String line;
        FileReader file;
        BufferedReader reader;

        file = new FileReader(itemsFileName);
        reader = new BufferedReader(file);
        while ((line = reader.readLine()) != null) {
            String[] values = line.split(" ");
            wordId = Integer.valueOf(values[0]);

            itemsId.add(wordId);
        }
        reader.close();
    }

    private void writeTransactionData() throws IOException {
        int docCount = 0, docTotal = 0;
        HashMap<Integer, Integer> transaction;
        FileOutputStream file = new FileOutputStream("transactions.dat");
        BufferedOutputStream buffer = new BufferedOutputStream(file);
        DataOutputStream writer = new DataOutputStream(buffer);

        System.out.println("Writing header");

        writer.writeInt(documentsId.size());
        System.out.println(documentsId.size());
        for (Integer docId : documentsId) {
            writer.writeInt(docId);
        }

        writer.writeInt(itemsId.size());
        System.out.println(itemsId.size());
        for (Integer itemId : itemsId) {
            writer.writeInt(itemId);
        }

        System.out.println("Writing data");

        for (Integer docId : documentsId) {
            if (docCount >= 10000) {
                docTotal += docCount;
                docCount = 0;
                System.out.println(docTotal + "/" + documentsId.size());
            }
            docCount++;

            transaction = transactions.get(docId);
            writer.writeInt(transaction.size());
            for (Map.Entry<Integer, Integer> entry : transaction.entrySet()) {
                writer.writeInt(itemsId.indexOf(entry.getKey()));
                writer.writeInt(entry.getKey());
            }
        }
        writer.close();
    }
}

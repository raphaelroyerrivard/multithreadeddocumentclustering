import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

public class HierarchicalClustering {
    private static final long seed = 1361190652140721L;
    static int CLUSTER_ID = 1;
    static float MIN_REPRESENTANTS_AVG = 0.50f;
    static int THREADS = 8;
    static int NSF_PROGRAMS = 632;
    static int KMEANS_MAX_ITERATIONS = 30;
    static float KMEANS_MIN_CHANGE_PERCENTAGE = 0.01f;
    static int BEST_WORDS_COUNT = 5;
    static List<String> docPrograms = new ArrayList<>();
    static HashMap<Integer, String> words = new HashMap<>();

    public static void main(String[] args) {
        try {
            List<Cluster> clusters = new ArrayList<>();
            HashMap<Integer, Doc> docs = new HashMap<>();
            FileReader file;
            BufferedReader reader;
            String[] lineValues;
            String line, programId;
            int docId, wordId, frequency;
            Doc doc;
            Cluster clusterToSplit;
            Cluster[] splitCluster;
            long divisiveIterationTime;
            long totalTime = System.currentTimeMillis();

            System.out.println("Reading data file");
            file = new FileReader("../cleaned_doc_words_by_frequency.txt");
            reader = new BufferedReader(file);
            while ((line = reader.readLine()) != null) {
                lineValues = line.split(" ");
                docId = Integer.valueOf(lineValues[0]);
                wordId = Integer.valueOf(lineValues[1]);
                frequency = 1;
                doc = docs.getOrDefault(docId, new Doc(docId, new HashMap<>()));
                doc.words.put(wordId, frequency);
                docs.put(docId, doc);
            }

            System.out.println("Starting clustering");
            //initial cluster containing all docs
            clusters.add(new Cluster(CLUSTER_ID++, new HashSet<>(docs.values())));
            boolean purityAchieved = false;
            while(!purityAchieved) {
                System.out.println("------------- Iteration " + clusters.size());
                divisiveIterationTime = System.currentTimeMillis();
                clusterToSplit = null;
                System.out.println("Finding cluster to split");
                for(Cluster cluster : clusters) {
                    if(cluster.getAverageOccurrenceOfBestWords() < MIN_REPRESENTANTS_AVG) {
                        clusterToSplit = cluster;
                        break;
                    }
                }
                if(clusterToSplit == null) {
                    purityAchieved = true;
                } else {
                    System.out.println("Splitting cluster " + clusterToSplit.toString());
                    splitCluster = clusterToSplit.split();
                    clusters.remove(clusterToSplit);
                    clusters.add(splitCluster[0]);
                    clusters.add(splitCluster[1]);
                }
                System.out.println("Took " + (System.currentTimeMillis() - divisiveIterationTime) / 1000.f + " seconds to run");
            }

            System.out.println("Reading docprograms file");
            file = new FileReader("../docprograms.txt");
            reader = new BufferedReader(file);
            while ((line = reader.readLine()) != null) {
                lineValues = line.split(" ");
                programId = lineValues.length > 1 ? lineValues[1] : "";
                docPrograms.add(programId);
            }

            System.out.println("Reading words file");
            file = new FileReader("../words.txt");
            reader = new BufferedReader(file);
            while ((line = reader.readLine()) != null) {
                lineValues = line.split(" ");
                words.put(Integer.parseInt(lineValues[0]), lineValues[1]);
            }

            System.out.println("Results (ordered by SNSFP):");
            List<Cluster> clustersBySNSFP = clusters.stream()
                    .sorted(Comparator.comparingDouble(c -> (double)c.getNSFProgramsScore()))
                    .collect(Collectors.toList());
            Collections.reverse(clustersBySNSFP);
            for(int i=0; i<clustersBySNSFP.size(); ++i) {
                Cluster cluster = clustersBySNSFP.get(i);
                cluster.getCenter();    //not sure if useful
                System.out.println(i + " " + cluster.toStringResult());
            }

            System.out.println("Results (ordered by PFMMPF):");
            List<Cluster> clustersByPFMMPF = clusters.stream()
                    .sorted(Comparator.comparingDouble(c -> (double)c.getAverageOccurrenceOfBestWords()))
                    .collect(Collectors.toList());
            Collections.reverse(clustersByPFMMPF);
            for(int i=0; i<clustersByPFMMPF.size(); ++i) {
                Cluster cluster = clustersByPFMMPF.get(i);
                cluster.getCenter();    //not sure if useful
                System.out.println(i + " " + cluster.toStringResult());
            }

            System.out.println("Results (ordered by size):");
            List<Cluster> clustersBySize = clusters.stream()
                    .sorted(Comparator.comparingInt(c -> c.docs.size()))
                    .collect(Collectors.toList());
            Collections.reverse(clustersBySize);
            float avgSNSFP = 0;
            float avgPFMMPF = 0;
            int totalCount = 0;
            for(int i=0; i<clustersBySize.size(); ++i) {
                Cluster cluster = clustersBySize.get(i);
                cluster.getCenter();    //not sure if useful
                System.out.println(i + " " + cluster.toStringResult());
                avgSNSFP += cluster.getNSFProgramsScore() * cluster.docs.size();
                avgPFMMPF += cluster.getAverageOccurrenceOfBestWords() * cluster.docs.size();
                totalCount += cluster.docs.size();
            }
            avgSNSFP /= totalCount;
            avgPFMMPF /= totalCount;
            System.out.println("Average SNSFP: " + avgSNSFP);
            System.out.println("Average PFMMPF: " + avgPFMMPF);

            System.out.println("Total time: " + (System.currentTimeMillis() - totalTime) / 60000.f + " minutes");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class Cluster {
        public int id;
        public HashSet<Doc> docs;
        private HashMap<Integer, Double> center;

        public Cluster(int id, HashSet<Doc> docs) {
            this.id = id;
            this.docs = docs;
            center = null;
        }

        public Cluster(int id, HashMap<Integer, Double> center) {
            this.id = id;
            this.docs = new HashSet<>();
            this.center = center;
        }

        public HashMap<Integer, Double> getCenter() {
            if(center == null)
                calcCenter();
            return center;
        }

        private void calcCenter() {
            HashMap<Integer, Double> center = new HashMap<>();
            for(Doc doc : docs) {
                for(Map.Entry<Integer, Integer> wordFrequency : doc.words.entrySet()) {
                    double sum = center.getOrDefault(wordFrequency.getKey(), 0.0);
                    center.put(wordFrequency.getKey(), sum + wordFrequency.getValue());
                }
            }
            for(Map.Entry<Integer, Double> wordSum : center.entrySet()) {
                center.put(wordSum.getKey(), wordSum.getValue() / docs.size());
            }
            this.center = center;
        }


        private double similitude(Doc doc) {
            double similitude = 0.0;
            for (Map.Entry<Integer, Integer> word : doc.words.entrySet()) {
                similitude += center.getOrDefault(word.getKey(), 0.0);
            }
            similitude /= doc.words.size();
            return similitude;
        }

        private Integer reaffectedDocs;
        public Cluster[] split() throws Exception {
            Cluster[] results = new Cluster[2];
            Random random = new Random(seed);
            int rand1 = random.nextInt(docs.size()), rand2;
            while ((rand2 = random.nextInt(docs.size())) == rand1)
                System.out.println("Second random is same as first, reroll");
            Cluster cluster1 = new Cluster(CLUSTER_ID++, ((Doc) docs.toArray()[rand1]).toCenter());
            Cluster cluster2 = new Cluster(CLUSTER_ID++, ((Doc) docs.toArray()[rand2]).toCenter());
            int iteration = 0;
            do {
                System.out.println("K-Mean iteration " + ++iteration + " with " + docs.size() + " docs");
                reaffectedDocs = 0;
                Thread[] threads = new Thread[THREADS];
                Doc[] docsArray = new Doc[docs.size()];
                docs.toArray(docsArray);
                for (int i = 0; i < THREADS; ++i) {
                    final int finalI = i;
                    threads[i] = new Thread(() -> {
                        Doc doc;
                        boolean contains;
                        double similitude1, similitude2;
                        int localReaffectedDocs = 0;
                        for (int j = finalI; j < docs.size(); j += THREADS) {
                            doc = docsArray[j];
                            similitude1 = cluster1.similitude(doc);
                            similitude2 = cluster2.similitude(doc);
                            if (similitude1 > similitude2 || (similitude1 == similitude2 && cluster1.docs.size() <= cluster2.docs.size())) {
                                synchronized (cluster1) { contains = cluster1.docs.contains(doc); }
                                //if not already in cluster1
                                if (!contains) {
                                    synchronized (cluster2) { cluster2.docs.remove(doc); }
                                    synchronized (cluster1) { cluster1.docs.add(doc); }
                                    ++localReaffectedDocs;
                                }
                            } else {
                                synchronized (cluster2) { contains = cluster2.docs.contains(doc); }
                                //if not already in cluster2
                                if (!contains) {
                                    synchronized (cluster1) { cluster1.docs.remove(doc); }
                                    synchronized (cluster2) { cluster2.docs.add(doc); }
                                    ++localReaffectedDocs;
                                }
                            }
                        }
                        synchronized (this) { reaffectedDocs += localReaffectedDocs; }
                    });
                    threads[i].start();
                }
                //wait for all threads to be finished
                for (int i = 0; i < THREADS; ++i) {
                    try {
                        threads[i].join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(reaffectedDocs + " reaffected docs");
                if(cluster1.docs.size() == 0 || cluster2.docs.size() == 0) {
                    throw new Exception("One of the two clusters is empty");
                }
                System.out.println("Calculating new centers");
                cluster1.calcCenter();
                cluster2.calcCenter();
            } while (reaffectedDocs > KMEANS_MIN_CHANGE_PERCENTAGE * docs.size() && iteration < KMEANS_MAX_ITERATIONS);

            results[0] = cluster1;
            results[1] = cluster2;
            System.out.println("K-Mean finished");
            return results;
        }

        public int getProgramsCount() {
            HashSet<String> programs = new HashSet<>();
            for(Doc currentDoc : docs) {
                programs.add(docPrograms.get(currentDoc.id - 1));
            }
            return programs.size();
        }

        public float getNSFProgramsScore() {
            int programs = getProgramsCount() - 1;
            int max = Math.max(1, Math.min(docs.size(), NSF_PROGRAMS) - 1);
            return 1 - (float)programs / max;
        }

        public List<Map.Entry<Integer, Double>> getBestWords(int count) {
            return getCenter().entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                    .collect(Collectors.toList())
                    .subList(0, count);
        }

        public float getAverageOccurrenceOfBestWords() {
            int count = Math.min(getCenter().size(), BEST_WORDS_COUNT);
            List<Map.Entry<Integer, Double>> bestWords = getBestWords(count);
            float avg = 0;
            for(Map.Entry<Integer, Double> word : bestWords)
                avg += word.getValue();
            return  avg / count;
        }

        private String getBestWordsString() {
            int count = Math.min(center.size(), BEST_WORDS_COUNT);
            List<Map.Entry<Integer, Double>> bestWords = getBestWords(count);
            String bestWordsString = "[ ";
            for(Map.Entry<Integer, Double> word : bestWords) {
                bestWordsString += "(" + words.get(word.getKey()) + ":" + (int)(word.getValue() * 100) + "%), ";
            }
            return bestWordsString.substring(0, bestWordsString.length() - 2) + " ]";
        }

        public String toString() {
            return String.format("<Cluster id:%s, docs:%s, bestWordsAvg:%s%%>", id, docs.size(), (int) (getAverageOccurrenceOfBestWords() * 100));
        }

        public String toStringResult() {
            int maxPrograms = Math.min(docs.size(), 632);
            int wordsPurity = (int) (getAverageOccurrenceOfBestWords() * 100);
            int programPurity = (int) (getNSFProgramsScore() * 100);
            return String.format("<Cluster id:%s, docs:%s, programs:%s/%s (%s%%), bestWordsAvg:%s%%, bestWords:%s>",
                    id, docs.size(), getProgramsCount(), maxPrograms, programPurity, wordsPurity, getBestWordsString());
        }
    }

    public static class Doc {
        public int id;
        public HashMap<Integer, Integer> words;

        public Doc(int id, HashMap<Integer, Integer> words) {
            this.id = id;
            this.words = words;
        }

        public HashMap<Integer, Double> toCenter() {
            HashMap<Integer, Double> center = new HashMap<>();
            for(Integer word : words.keySet())
                center.put(word, 1.0);
            if(center.size() == 0)
                System.out.println("Empty center WTF");
            return center;
        }
    }
}

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class ResultAnalyzer {
    private static final String transactionsFileName = "transactions.dat";
    private static final String resultsFileName = "../result_kmeans.dat";
    private static final String programsFileName = "../docprograms.txt";
    private static final String clusterDescriptionFormat =
            "<Cluster %d docs:%d programs:%d SNSFP:%.0f%% PFMMPF:%.0f%% bestWords:%s>";
    private static final int maxClusterWords = 5;

    private static int[] documentsId, wordsId;

    private static int clusterCount;
    private static List<Set<Integer>> clustersDocuments;
    private static List<Set<Integer>> clustersWords;
    private static Map<Integer, Set<Integer>> documentsWords;

    private static List<String> programsId;
    private static Map<Integer, String> words;

    public static void main(String[] args) {
        try {
            readTransactionData();
            readResults();
            // we are donne with these
            wordsId = documentsId = null;
            readDocPrograms();
            readWords();
            analyzeClusters();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //--------------------------------------------------------------------------
    // Initialization

    private static void readTransactionData() throws IOException {
        FileInputStream file = new FileInputStream(transactionsFileName);
        BufferedInputStream buffer = new BufferedInputStream(file);
        DataInputStream reader = new DataInputStream(buffer);

        int documentCount, itemCount;

        documentCount = reader.readInt();
        documentsId = new int[documentCount];
        for (int i = 0; i < documentCount; i++) {
            documentsId[i] = reader.readInt();
        }

        itemCount = reader.readInt();
        wordsId = new int[itemCount];
        for (int i = 0; i < itemCount; i++) {
            wordsId[i] = reader.readInt();
        }


        documentsWords = new HashMap<>();
        Set<Integer> words;
        for (int i = 0; i < documentCount; ++i) {
            words = new HashSet<>();
            int nbEntry = reader.readInt();
            for (int j = 0; j < nbEntry; ++j) {
                int wordIndex = reader.readInt();
                int value = reader.readInt();
                if (value > 0)
                    words.add(wordsId[wordIndex]);
            }
            documentsWords.put(documentsId[i], words);
        }
        reader.close();
    }

    private static void readResults() throws IOException {
        FileInputStream file = new FileInputStream(resultsFileName);
        BufferedInputStream buffer = new BufferedInputStream(file);
        DataInputStream reader = new DataInputStream(buffer);

        int documentCount, itemCount;

        Set<Integer> words;
        clustersWords = new ArrayList<>();
        clustersDocuments = new ArrayList<>();
        clusterCount = reader.readInt();
        itemCount = reader.readInt();
        for (int i = 0; i < clusterCount; ++i) {
            words = new HashSet<>();
            for (int j = 0; j < itemCount; ++j) {
                float value = reader.readFloat();
                if (value > 0)
                    words.add(wordsId[j]);
            }
            clustersWords.add(words);
            clustersDocuments.add(new HashSet<>());
        }

        documentCount = reader.readInt();
        for (int docIndex = 0; docIndex < documentCount; docIndex++) {
            int clusterId = reader.readInt();
            clustersDocuments.get(clusterId).add(documentsId[docIndex]);
        }
        reader.close();
    }

    private static void readDocPrograms() throws IOException {
        FileReader file = new FileReader(programsFileName);
        BufferedReader reader = new BufferedReader(file);
        String line;
        String[] lineValues;
        String programId;
        programsId = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            lineValues = line.split(" ");
            programId = lineValues.length > 1 ? lineValues[1] : "";
            programsId.add(programId);
        }
    }

    private static void readWords() throws IOException {
        FileReader file = new FileReader("../words.txt");
        BufferedReader reader = new BufferedReader(file);
        String line;
        String[] lineValues;
        words = new HashMap<>();
        while ((line = reader.readLine()) != null) {
            lineValues = line.split(" ");
            words.put(Integer.parseInt(lineValues[0]), lineValues[1]);
        }
    }

    //--------------------------------------------------------------------------
    // Analysis

    private static void analyzeClusters() {
        List<MyEntry<Float, String>> clustersBySNSFP = new ArrayList<>();
        List<MyEntry<Float, String>> clustersByPFMMPF = new ArrayList<>();
        List<MyEntry<Integer, String>> clustersBySize = new ArrayList<>();

        for (int i = 0; i < clusterCount; ++i) {

            int bestWordCount = Math.min(getWordsInCluster(i), maxClusterWords);
            List<Map.Entry<Integer, Float>> bestWords = getBestWords(i, bestWordCount);

            float SNSFP = getNSFProgramsScore(i);
            float PFMMPF = getAverageOccurrenceOfBestWords(bestWords, bestWordCount);
            int clusterSize = getClusterSize(i);
            int clusterProgramCount = getProgramsCount(i);

            String clusterDesc = String.format(clusterDescriptionFormat,
                    i, clusterSize, clusterProgramCount, SNSFP * 100, PFMMPF * 100, getBestWordsString(bestWords));
            clusterDesc += "---" + clusterSize;

            clustersBySNSFP.add(new MyEntry<>(SNSFP, clusterDesc));
            clustersByPFMMPF.add(new MyEntry<>(PFMMPF, clusterDesc));
            clustersBySize.add(new MyEntry<>(clusterSize, clusterDesc));
        }

        clustersBySNSFP.sort((o1, o2) -> Float.compare(o2.getKey(), o1.getKey()));
        clustersByPFMMPF.sort((o1, o2) -> Float.compare(o2.getKey(), o1.getKey()));
        clustersBySize.sort((o1, o2) -> Integer.compare(o2.getKey(), o1.getKey()));

        System.out.println("Results by SNSFP:");
        float avgSNSFP = 0;
        for(Map.Entry<Float, String> cluster : clustersBySNSFP) {
            System.out.println(cluster.getValue());
            avgSNSFP += cluster.getKey() * Integer.valueOf(cluster.getValue().split("---")[1]);
        }

        System.out.println("Results by PFMMPF:");
        float avgPFMMPF = 0;
        for (Map.Entry<Float, String> cluster : clustersByPFMMPF) {
            System.out.println(cluster.getValue());
            avgPFMMPF += cluster.getKey() * Integer.valueOf(cluster.getValue().split("---")[1]);
        }

        System.out.println("Results by size:");
        int clusterCountSum = 0;
        for(Map.Entry<Integer, String> cluster : clustersBySize) {
            System.out.println(cluster.getValue());
            clusterCountSum += cluster.getKey();
        }

        avgSNSFP /= clusterCountSum;
        System.out.println("Average SNSFP: " + avgSNSFP);
        avgPFMMPF /= clusterCountSum;
        System.out.println("Average PFMMPF: " + avgPFMMPF);
    }

    private static int getProgramsCount(int clusterIndex) {
        Set<String> programs = new HashSet<>();
        for (Integer docId : clustersDocuments.get(clusterIndex)) {
            programs.add(programsId.get(docId - 1));
        }
        return programs.size();
    }

    private static float getNSFProgramsScore(int clusterIndex) {
        int programs = getProgramsCount(clusterIndex) - 1;
        int max = Math.max(1, Math.min(clustersDocuments.get(clusterIndex).size(), 632) - 1);
        return 1 - (float) programs / max;
    }

    private static List<Map.Entry<Integer, Float>> getBestWords(int clusterIndex, int count) {
        Map<Integer, Float> bestWords = new HashMap<>();
        Set<Integer> clusterWords = clustersWords.get(clusterIndex);
        Set<Integer> clusterDocuments = clustersDocuments.get(clusterIndex);
        for (Integer wordId : clusterWords) {
            int wordCount = 0;
            for (Integer docId : clusterDocuments) {
                if (documentsWords.get(docId).contains(wordId))
                    ++wordCount;
            }
            bestWords.put(wordId, (float) wordCount / clusterDocuments.size());
        }
        return bestWords.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .collect(Collectors.toList())
                .subList(0, count);
    }

    private static float getAverageOccurrenceOfBestWords(List<Map.Entry<Integer, Float>> bestWords, int count) {
        float avg = 0;
        for (Map.Entry<Integer, Float> word : bestWords)
            avg += word.getValue();
        return avg / count;
    }

    private static int getWordsInCluster(int clusterIndex) {
        return clustersWords.get(clusterIndex).size();
    }

    private static int getClusterSize(int clusterIndex) {
        return clustersDocuments.get(clusterIndex).size();
    }

    private static String getBestWordsString(List<Map.Entry<Integer, Float>> bestWords) {
        StringBuilder bestWordsString = new StringBuilder("[ ");
        for (Map.Entry<Integer, Float> word : bestWords) {
            bestWordsString.append("(").append(words.get(word.getKey())).append(":").append((int) (word.getValue() * 100)).append("%), ");
        }
        return bestWordsString.substring(0, bestWordsString.length() - 2) + " ]";
    }

    final static class MyEntry<K, V> implements Map.Entry<K, V> {
        private final K key;
        private V value;

        MyEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V old = this.value;
            this.value = value;
            return old;
        }
    }
}
